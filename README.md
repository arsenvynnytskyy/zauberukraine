# README

Web-site with Ruby on Rails

* ruby-3.2.2, rails-7.0.3

* Installation steps:
    1. Install RVM (https://rvm.io/rvm/install):
        - gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
        - curl -sSL https://get.rvm.io | bash
    2. Intall ruby 3.2.2 into RVM:
        - rvm install ruby-3.2.2
    3. Clone Zauber Ukraine repository:
        - git clone git@gitlab.com:arsenvynnytskyy/zauberukraine.git
    4. In project folder run bundler:
        - bundler

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
