Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "welcome#index"

  get 'about', to: 'welcome#about'
  get 'main', to: 'articles#main'
  get 'parts', to: 'articles#parts'
  get 'services', to: 'articles#services'
  get 'contacts', to: 'articles#contacts'
end
